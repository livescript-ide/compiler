import
    \source-map
    \js-nodes/symbols : { as-node }

Super = Symbol \Super

original-add = source-map.SourceNode::add

source-map.SourceNode::add = (a-chunk) ->
    if a-chunk[as-node]
        original-add.call @, "#{a-chunk}"
    else
        original-add ...

#
``class SourceNode extends sourceMap.SourceNode {
    constructor(...args) {
        super(...args)
    }
}``

SourceNode.prototype <<<
    (Super): source-map.SourceNode.prototype

    set-file: (filename) ->
        @source = filename
        for child in @children when child instanceof source-map.SourceNode
            child.set-file filename
SourceNode <<<
    from-source-node: -> it with @prototype

export default SourceNode
